﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour {

    public float speed;
    public Text countText;
    public Text winText;
    public Text fuelText;
    public float jump;
    public float fuel;
    public GameObject engine;

    private Rigidbody rb;
    private int count;

    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
        jump = 0.0f;
        fuel = 0.0f;
        engine.SetActive(false);
    }


    private void Update() 
    {
        if(transform.position.y < -20)
        {
            RestartLevel();
        }
    }

    void FixedUpdate ()
    {
        //movement
        float moveHorizontal = Input.GetAxis ("Horizontal");
        float moveVertical = Input.GetAxis ("Vertical");
        Vector3 movement = new Vector3 (moveHorizontal, jump, moveVertical);
        rb.AddForce (movement * speed);

        //handling input for jumps
        if (Input.GetKey(KeyCode.Space) & fuel > 0)
        {
            jump = 5;
            fuel = fuel - 1;
            //turn on the game object that has the trail and particle system
            engine.SetActive(true);
        }
        else
        {
            //otherwise turn the engine off
            jump = 0;
            engine.SetActive(false);
        }
        //reasigning the text for the fuel
        fuelText.text = "Fuel: " + fuel.ToString();

    }

    void OnTriggerEnter(Collider other) 
    {   
        // player hits a pickup object
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
        
        //if the player hits a jump pick up
        if (other.gameObject.CompareTag("JumpPickup"))
        {
            //deactivat the object and give the player 25 fuel
            other.gameObject.SetActive(false);
            fuel = fuel + 25;
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        // if the player collides with 12 pickups display you win
        if (count >= 12)
        {
            winText.text = "You Win!!!";
        }
    }

    void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
