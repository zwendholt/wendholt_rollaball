﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngineController : MonoBehaviour
{
    public GameObject player;

    private Vector3 offset;

    void Start ()
    {
        // setting the position of the engine to be the player
        offset = transform.position - player.transform.position;
    }

    void LateUpdate ()
    {
        // keeping the position of the engine on the player
        transform.position = player.transform.position + offset;
    }
}
